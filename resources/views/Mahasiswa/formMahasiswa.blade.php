@extends('layout.layout')
@section('section')
    
<div class="container-fluid px-4">
    <h1 class="mt-4">Mahasiswa</h1>

    <div class="card mb-4">
        <div class="card-header d-flex align-items-center justify-content-between small">
            <div>
                <i class="fas fa-table me-1"></i>
                Data Mahasiswa
            </div>
            <div>
                <a href="/mhs/create" class="btn btn-primary "><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
            </div>

        </div>

        <div class="card-body">
            
              
            <table class="table table-striped table table-bordered">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>NIM</th>
                        <th>E-Mail</th>
                        <th>No Telp</th>
                        <th>Prodi</th>
                        <th>Jurusan</th>
                        <th>Fakultas</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($mahasiswa as $mhs)
                        <tr>
                            <td> {{ $mhs->nama }} </td>
                            <td> {{ $mhs->nim }} </td>
                            <td> {{ $mhs->email }} </td>
                            <td> {{ $mhs->no_telp }} </td>
                            <td> {{ $mhs->prodi }} </td>
                            <td> {{ $mhs->jurusan }} </td>
                            <td> {{ $mhs->fakultas }} </td>
                            <td style="text-align: center;" class="card-header d-flex align-items-center justify-content-between small"  >
                                <a href="/mhs/Mahasiswa/editMahasiswa/{{$mhs->id_mahasiswa}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="/mhs/delete/{{$mhs->id_mahasiswa}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

@endsection