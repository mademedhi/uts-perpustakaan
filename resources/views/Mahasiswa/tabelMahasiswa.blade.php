@extends('layout.layout')
@section('section')
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header"><h3 class="text-center font-weight-light my-4">Tambah Data Mahasiswa</h3></div>
                <div class="card-body">
                    <form action="/mhs/simpan" method="POST">
                        {{ csrf_field() }}
                        <div class="form-floating mb-3">
                            <input name="nama" class="form-control" type="text" placeholder="Nama Mahasiswa" />
                            <label>Nama Mahasiswa</label>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="nim" class="form-control" type="text" placeholder="NIM" />
                                    <label>NIM</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="email" class="form-control" id="inputLastName" type="text" placeholder="Email" />
                                    <label>Email</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="no_telp" class="form-control" type="text" placeholder="Nomor Telepon" />
                                    <label>Nomor Telepon</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="prodi" class="form-control" id="inputLastName" type="text" placeholder="Prodi" />
                                    <label>Prodi</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="jurusan" class="form-control" type="text" placeholder="Jurusan" />
                                    <label>Jurusan</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="fakultas" class="form-control" id="inputLastName" type="text" placeholder="Fakultas" />
                                    <label>Fakultas</label>
                                </div>
                            </div>
                        </div>

                        <div class="mt-4 mb-0">
                            <input class="d-grid btn btn-primary btn-block " href="" value="Create" type="submit">
                            {{-- <div class="d-grid"><a class="btn btn-primary btn-block" href="login.html">Tambah Data</a></div> --}}
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection