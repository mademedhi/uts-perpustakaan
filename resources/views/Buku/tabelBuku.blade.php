@extends('layout.layout')
@section('section')
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header"><h3 class="text-center font-weight-light my-4">Tambah Data Buku</h3></div>
                <div class="card-body">
                    <form action="/buku/simpan" method="POST">
                        {{ csrf_field() }}
                        <div class="form-floating mb-3">
                            <input name="judul_buku" value="Judul Buku" required="required" class="form-control" type="text" placeholder="Judul Buku" />
                            <label>Judul Buku</label>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="pengarang" value="Pengarang" required="required" class="form-control" type="text" placeholder="Pengarang" />
                                    <label>Pengarang</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="penerbit" value="Penerbit" required="required"  class="form-control" id="inputLastName" type="text" placeholder="Penerbit" />
                                    <label>Penerbit</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="tahun_terbit" value="Tahun Terbit" required="required"  class="form-control" type="date" placeholder="Tahun Terbit" />
                                    <label>Tahun Terbit</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="tebal" value="Tebal" required="required"  class="form-control"  type="text" placeholder="Tebal"/>
                                    <label>Tebal</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="isbn" value="ISBN" required="required"  class="form-control" type="text" placeholder="isbn" />
                                    <label>ISBN</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="stok_buku" value="Stok Buku" required="required"  class="form-control"  type="text" placeholder="Stok Buku"/>
                                    <label>Stok Buku</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="biaya_sewa_harian" value="Biaya Sewa Harian" required="required"  class="form-control" type="text" placeholder="Biaya Sewa Harian" />
                                    <label>Biaya Sewa Harian</label>
                                </div>
                            </div>
                        </div>

                        <div class="mt-4 mb-0">
                            <input class="d-grid btn btn-primary btn-block " href="" value="Create" type="submit">
                            {{-- <div class="d-grid"><a class="btn btn-primary btn-block" href="login.html">Tambah Data</a></div> --}}
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection