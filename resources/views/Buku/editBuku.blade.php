@extends('layout.layout')
@section('section')
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header"><h3 class="text-center font-weight-light my-4">Edit Data Mahasiswa</h3></div>
                <div class="card-body">
                    @foreach ( $buku as $book )
                    <form action="/buku/update/{{ $book->id_buku }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-floating mb-3">
                            <input name="judul_buku" value="{{$book->judul_buku}}" required="required" class="form-control" type="text" placeholder="Judul Buku" />
                            <label>Judul Buku</label>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="pengarang" value="{{$book->pengarang}}" required="required" class="form-control" type="text" placeholder="Pengarang" />
                                    <label>Pengarang</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="penerbit" value="{{$book->penerbit}}" required="required"  class="form-control" id="inputLastName" type="text" placeholder="Penerbit" />
                                    <label>Penerbit</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="tahun_terbit" value="{{$book->tahun_terbit}}" required="required"  class="form-control" type="dat e" placeholder="Tahun Penerbit" />
                                    <label>Tahun Penerbit</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="tebal" value="{{$book->tebal}}" required="required"  class="form-control"  type="text" placeholder="Tebal"/>
                                    <label>Tebal</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="isbn" value="{{$book->isbn}}" required="required"  class="form-control" type="text" placeholder="isbn" />
                                    <label>ISBN</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input name="stok_buku" value="{{$book->stok_buku}}" required="required"  class="form-control"  type="text" placeholder="Stok Buku"/>
                                    <label>Stok Buku</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="biaya_sewa_harian" value="{{$book->biaya_sewa_harian}}" required="required"  class="form-control" type="text" placeholder="Biaya Sewa Harian" />
                                    <label>Biaya Sewa Harian</label>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 mb-0">
                            <input class="d-grid btn btn-primary btn-block " value="Update" type="submit">
                            {{-- <div class="d-grid"><a class="btn btn-primary btn-block" href="login.html">Tambah Data</a></div> --}}
                        </div>
                    </form>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>

@endsection