@extends('layout.layout')
@section('section')
    
<div class="container-fluid px-4">
    <h1 class="mt-4">Buku</h1>

    <div class="card mb-4">
        <div class="card-header d-flex align-items-center justify-content-between small">
            <div>
                <i class="fas fa-table me-1"></i>
                Data Buku
            </div>
            <div>
                <a href="/buku/create" class="btn btn-primary "><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
            </div>

        </div>

        <div class="card-body">
            <table class="table table-striped table table-bordered">
                <thead>
                    <tr>
                        <th>Judul Buku</th>
                        <th>Pengarang</th>
                        <th>Penerbit</th>
                        <th>Tahun Terbit</th>
                        <th>Tebal</th>
                        <th>ISBN</th>
                        <th>Stok Buku</th>
                        <th>Biaya Sewa</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($buku as $book)
                        <tr>
                            <td> {{ $book->judul_buku }} </td>
                            <td> {{ $book->pengarang }} </td>
                            <td> {{ $book->penerbit }} </td>
                            <td> {{ $book->tahun_terbit }} </td>
                            <td> {{ $book->tebal }} </td>
                            <td> {{ $book->isbn }} </td>
                            <td> {{ $book->stok_buku }} </td>
                            <td> {{ $book->biaya_sewa_harian }} </td>
                            <td style="text-align: center;" class="card-header d-flex align-items-center justify-content-between small"  >
                                <a href="/buku/Buku/editBuku/{{$book->id_buku}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="/buku/delete/{{$book->id_buku}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

@endsection