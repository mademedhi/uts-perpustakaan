<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/mhs', [App\Http\Controllers\MahasiswaCont::class,'readData']);
Route::get('/mhs/create', [App\Http\Controllers\MahasiswaCont::class,'addData']);
Route::post('/mhs/simpan', [App\Http\Controllers\MahasiswaCont::class,'create']);
Route::get('/mhs/delete/{id}',[App\Http\Controllers\MahasiswaCont::class,'delete']);
Route::get('/mhs/Mahasiswa/editMahasiswa/{id}',[App\Http\Controllers\MahasiswaCont::class,'edit']);
Route::post('/mhs/update/{id}',[App\Http\Controllers\MahasiswaCont::class,'update']);

Route::get('/buku', [App\Http\Controllers\BukuCont::class,'readData']);
Route::get('/buku/create', [App\Http\Controllers\BukuCont::class,'addData']);
Route::post('/buku/simpan', [App\Http\Controllers\BukuCont::class,'create']);
Route::get('/buku/delete/{id}',[App\Http\Controllers\BukuCont::class,'delete']);
Route::get('/buku/Buku/editBuku/{id}',[App\Http\Controllers\BukuCont::class,'edit']);
Route::post('/buku/update/{id}',[App\Http\Controllers\BukuCont::class,'update']);