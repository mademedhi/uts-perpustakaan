<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuCont extends Controller
{
    public function readData()
    {

        $buku = DB::table('buku')->get();

        return view('/Buku/formBuku', ['buku' => $buku]);
    }
    public function addData()
    {
        return view('/Buku/tabelBuku');
    }

    public function create(Request $request)
    {
        DB::table('buku')->insert([
            'judul_buku' => $request->judul_buku,
            'pengarang' => $request->pengarang,
            'penerbit' => $request->penerbit,
            'tahun_terbit' => $request->tahun_terbit,
            'tebal' => $request->tebal,
            'isbn' => $request->isbn,
            'stok_buku' => $request->stok_buku,
            'biaya_sewa_harian' => $request->biaya_sewa_harian,
        ]);
        return redirect('/buku');
    }

    public function edit($id)
    {
        $buku = DB::table('buku')-> where('id_buku', $id)->get();
        return view('/Buku/editBuku',['buku'=> $buku]);
    }

    public function update(Request $request,Int $id)
    {
        DB::table('buku')->where('id_buku', $id)->update([
            'judul_buku' => $request->judul_buku,
            'pengarang' => $request->pengarang,
            'penerbit' => $request->penerbit,
            'tahun_terbit' => $request->tahun_terbit,
            'tebal' => $request->tebal,
            'isbn' => $request->isbn,
            'stok_buku' => $request->stok_buku,
            'biaya_sewa_harian' => $request->biaya_sewa_harian,
        ]);

        return redirect('/buku');
    }

    public function delete($id){
        DB::table('buku')->where('id_buku', $id)->delete();

        return redirect('/buku');
    }

   
    

}
