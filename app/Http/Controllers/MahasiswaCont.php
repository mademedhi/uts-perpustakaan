<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaCont extends Controller
{
    public function readData()
    {

        $mahasiswa = DB::table('mahasiswa')->get();

        return view('/Mahasiswa/formMahasiswa', ['mahasiswa' => $mahasiswa]);
    }
    public function addData()
    {
        return view('/Mahasiswa/tabelmahasiswa');
    }

    public function create(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'prodi' => $request->prodi,
            'jurusan' => $request->jurusan,
            'fakultas' => $request->fakultas,
        ]);
        return redirect('/mhs');
    }

    public function edit($id)
    {
        $mahasiswa = DB::table('mahasiswa')-> where('id_mahasiswa', $id)->get();
        return view('/Mahasiswa/editMahasiswa',['mahasiswa'=> $mahasiswa]);
    }

    public function update(Request $request,Int $id)
    {
        DB::table('mahasiswa')->where('id_mahasiswa', $id)->update([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'prodi' => $request->prodi,
            'jurusan' => $request->jurusan,
            'fakultas' => $request->fakultas,
        ]);

        return redirect('/mhs');
    }

    public function delete($id){
        DB::table('mahasiswa')->where('id_mahasiswa', $id)->delete();

        return redirect('/mhs');
    }

   
}